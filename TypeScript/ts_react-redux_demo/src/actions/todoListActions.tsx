export const ADD_TODO = "todos:addTodo";
export const REMOVE_TODO = "todos:removeTodo";
export const MARK_DONE = "todos:markDone";
export const ERROR = "todos:error";

// An action must ALWAYS have a type. (payload is optional, but usually needed)
export function addTodoAction(newTodoTitle) {
	return {
		type: ADD_TODO,
		payload: { todoTitle: newTodoTitle },
	};
}

export function removeTodoAction(id) {
	return {
		type: REMOVE_TODO,
		payload: { id },
	};
}

export function markDoneAction(id) {
	return {
		type: MARK_DONE,
		payload: { id },
	};
}

export function errorAction(error) {
	return {
		type: ERROR,
		payload: { error },
	};
}

// Redux-thunk allows for an action to return a function. This function (instead of action object)
// is caught by thunk middleware. This function has a dispatch arg passed. This allows us to delay
// a dispatch / dispatch asynchronously, and only dispatch should a condition be met
export function fetchTodosAction() {
	return async (dispatch) => {
		try {
			let req = await fetch("https://jsonplaceholder.typicode.com/todos/");
			const todos = await req.json();
			for (let i = 0; i < todos.length; ++i) {
				setTimeout(() => dispatch(addTodoAction(todos[i].title)), 0);
			}
		} catch (e) {
			const error = `ERROR: could not fetch --- ${e}`;
			dispatch(errorAction(error));
		}
	};
}
