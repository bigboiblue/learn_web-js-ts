// These files are hiphonated instead of camelCase as they are passed
// as props

export const UPDATE_USER = "users:updateUser";

// An action must ALWAYS have a type. (payload is optional, but usually needed)
export function updateUserAction(newUsers) /* eslint-disable-line */ {
	// return an action
	return {
		type: UPDATE_USER,
		payload: newUsers,
	};
}
