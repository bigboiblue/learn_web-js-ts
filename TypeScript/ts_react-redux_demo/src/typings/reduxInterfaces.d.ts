interface User {
	name: string,
	age: number
}

interface Product {
	title: string,
	price: number,
}

interface action {
	type: string,
	payload: User | User[] | Product | Product[],
}
