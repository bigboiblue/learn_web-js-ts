import { UPDATE_USER } from "../actions/userActions";

// Use better static type checking with interfaces here...
// Takes state and action. State NEEDS a default value.
// This is because when redux inits, it dispatches a dummy action to fill the state
function userReducer(state: User[] = [], { type, payload }) {
	switch (type) {
	case UPDATE_USER:
		console.log(state);
		return payload.user;
	case "incrementAge":
		++state[0].age;
		return state;
	default:
		return state;
	}
}

export default userReducer;
