import {
	ADD_TODO, REMOVE_TODO, MARK_DONE, ERROR,
} from "../actions/todoListActions";

// Instead of a long annoying switch, we could use combineReducers (but im too lazy)
export default function todoReducer (state = [], { type, payload }): any {
	switch (type) {
	case ADD_TODO: {
		const newTodo = {
			id: state.length + 1,
			title: payload.todoTitle,
			complete: false,
		};
		// state.push(newTodo); NEVER MODIFY ACTUAL UNDERLYING EXISTING STATE
		// IF YOU DO THIS, REDUX DOES NOT DETECT A DIFFERENCE IN STATE
		const newState = [...state, newTodo];
		return newState;
	}
	case REMOVE_TODO: {
		const temp = state.filter(todo => todo.id !== payload.id);

		temp.forEach((val, index) => (val.id = index + 1));// eslint-disable-line

		return temp;
	}

	case MARK_DONE: {
		const newState = state.map((todo) => {
			if (todo.id === payload.id) {
				todo.completed = !todo.completed;
			}
			return todo;
		});
		return newState;
	}

	case ERROR: {
		console.log(`ERROR: ${payload.error}`);
		return state;
	}

	default:
		return state;
	}
}

// A selector is simply a function used to get derived data from the store
// (rather than storing it and making our store inneffecient and large)
// Selectors are usually placed alongside corresponding reducers.
// In the reducer file, they will only take the state corresponding to that reducer (e.g. todos)
// not the whole state... Then, we make a selector in index which simply calls this selector which
// takes the full state, and calls this with the corresponding sub state. This means we only need to
// import the index file, only can simply pass in the entire state...
export const selectFullStrings = (todos) => {
	const fullStrings = todos.map(todo => `${todo.id}: ${todo.title} [Complete: ${(todo.completed ? "✔" : "✘")}]`);
	return fullStrings;
};
