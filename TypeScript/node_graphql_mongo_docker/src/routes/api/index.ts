import express from "express";
import { GraphQLObjectType, GraphQLString, GraphQLSchema } from "graphql";
import graphqlHTTP from "express-graphql";
import authorsRouter from "./authors";
import booksRouter from "./books";
const router = express.Router();

router.use("/authors", authorsRouter);
router.use("/books", booksRouter);
router.use("/graphiql", graphqlHTTP({
    graphiql: true,
    schema,
}));

export default router;
