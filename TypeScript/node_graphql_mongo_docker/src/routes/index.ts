import express from "express";
import {
    GraphQLObjectType as QLObject,
    GraphQLString as QLString,
    GraphQLSchema as QLSchema,
    GraphQLInt as QLInt,
    GraphQLList as QLList,
    GraphQLNonNull as QLNonNull,
} from "graphql";
import graphqlHTTP from "express-graphql";
import mongoose from "mongoose";
import { ObjectId } from "mongodb";
const router = express.Router();

mongoose.connect("mongodb://localhost/book_auth", { useNewUrlParser: true });

//// INIT MONGOOSE MODELS AND SCHEMAS ////
const bookSchema = new mongoose.Schema({
    name: String,
    authorId: Number,
}, { versionKey: false });
const authorSchema = new mongoose.Schema({
    authorId: { type: Number, auto: true },
    name: { type: String, default: "No Author" },
}, { versionKey: false });

const books = mongoose.model("book", bookSchema);
const authors = mongoose.model("author", authorSchema);


//// INIT GRAPHQL SCHEMA AND ASSOCIATED ////
const BookType = new QLObject({
    name: "book",
    description: "This represents a book stored in the database",
    fields: () => ({
        // Resolved through db find()
        name: {
            type: QLNonNull(QLString),
            description: "The name of the book",
        },
        authorId: {
            type: QLNonNull(QLInt),
            description: "The ID of the author who wrote the book",
        },
        // Resolved through extra querys and calculation
        author: {
            type: AuthorType,
            description: "This is the author object of the author who wrote this book. (Note: This is resolved through and extra database query)",
            // parent argument passes the book object that contains already resolved properties
            // (contains the properties instantly evaluated through find())
            resolve: parent => authors.findOne({ authorId: parent.authorId }),
        },
    }),
});

const AuthorType = new QLObject({
    name: "author",
    description: "This represents an author stored in the database",
    fields: () => ({
        // Resolved through db find()
        name: {
            type: QLNonNull(QLString),
            description: "The name of the author",
        },
        authorId: {
            type: QLNonNull(QLInt),
            description: "The authors unique identifying id",
        },
        books: {
            type: QLList(BookType),
            description: "A list of books made by the author. (Note: This is resolved through and extra database query)",
            resolve: parent => books.find({ authorId: parent.authorId }),
        },
    }),
});

const rootQueryType = new QLObject({
    name: "root_query",
    description: "This is the root query for node_graphql_mongo_docker api",
    fields: () => ({
        book: {
            type: BookType,
            description: "Returns the book with the id passed",
            args: {
                id: { type: QLString },
            },
            resolve: (parent, id: any) => {
                const oid = new ObjectId(id);
                return books.findOne({ ObjectId: oid });
            },
        },
        books: {
            type: QLList(BookType),
            description: "Returns a list of all books",
            resolve: () => books.find(),
        },
        author: {
            type: AuthorType,
            description: "Returns the author that matches the authorId passed",
            args: {
                authorId: { type: QLInt },
            },
            resolve: (parent, authorId) => authors.findOne({ authorId }),
        },
        authors: {
            type: QLList(AuthorType),
            description: "Returns all authors",
            resolve: () => authors.find(),
        },
    }),
});

const rootMutationType = new QLObject({
    name: "root_mutation",
    description: "This is the root mutation for node_graphql_mongo_docker api",
    fields: (): any => ({
        addBook: {
            type: BookType,
            description: "Adds a book to the db. Takes name and authorId args (both non-null). Returns the added book.",
            args: {
                name: {
                    type: QLNonNull(QLString),
                    description: "Name of the new book",
                },
                authorId: {
                    type: QLNonNull(QLInt),
                    description: "New books authorId",
                },
            },
            resolve: (parent, args) => {
                const newBook = { name: args.name, authorId: args.authorId };
                books.create(newBook).then();
                return newBook;
            },
        },
    }),
});

const schema = new QLSchema({
    query: rootQueryType,
    mutation: rootMutationType,
});

router.use("/api", graphqlHTTP({
    graphiql: true,
    schema,
}));

export default router;
