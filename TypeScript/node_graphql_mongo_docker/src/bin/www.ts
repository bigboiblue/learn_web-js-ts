import app from "../app";

//// Any Startup Scripts Here ////

const PORT = process.env.PORT || 8080;
app.listen(PORT, (): void => console.log(`Server running on port: ${PORT}`));

//// Server error handling here ////
//// e.g.  app.on('error', onError); ////
