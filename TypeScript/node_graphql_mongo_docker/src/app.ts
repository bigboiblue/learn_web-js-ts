import express from "express";
import routesRouter from "./routes";
const app = express();

app.use(routesRouter);
app.use(express.static("../public"));

export default app;
