"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = _interopRequireDefault(require("express"));

var _graphql = require("graphql");

var _expressGraphql = _interopRequireDefault(require("express-graphql"));

var _mongoose = _interopRequireDefault(require("mongoose"));

var _mongodb = require("mongodb");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const router = _express.default.Router();

_mongoose.default.connect("mongodb://localhost/book_auth", {
  useNewUrlParser: true
}); //// INIT MONGOOSE MODELS AND SCHEMAS ////


const bookSchema = new _mongoose.default.Schema({
  name: String,
  authorId: Number
}, {
  versionKey: false
});
const authorSchema = new _mongoose.default.Schema({
  authorId: {
    type: Number,
    auto: true
  },
  name: {
    type: String,
    default: "No Author"
  }
}, {
  versionKey: false
});

const books = _mongoose.default.model("book", bookSchema);

const authors = _mongoose.default.model("author", authorSchema); //// INIT GRAPHQL SCHEMA AND ASSOCIATED ////


const BookType = new _graphql.GraphQLObjectType({
  name: "book",
  description: "This represents a book stored in the database",
  fields: () => ({
    // Resolved through db find()
    name: {
      type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLString),
      description: "The name of the book"
    },
    authorId: {
      type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLInt),
      description: "The ID of the author who wrote the book"
    },
    // Resolved through extra querys and calculation
    author: {
      type: AuthorType,
      description: "This is the author object of the author who wrote this book. (Note: This is resolved through and extra database query)",
      // parent argument passes the book object that contains already resolved properties
      // (contains the properties instantly evaluated through find())
      resolve: parent => authors.findOne({
        authorId: parent.authorId
      })
    }
  })
});
const AuthorType = new _graphql.GraphQLObjectType({
  name: "author",
  description: "This represents an author stored in the database",
  fields: () => ({
    // Resolved through db find()
    name: {
      type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLString),
      description: "The name of the author"
    },
    authorId: {
      type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLInt),
      description: "The authors unique identifying id"
    },
    books: {
      type: (0, _graphql.GraphQLList)(BookType),
      description: "A list of books made by the author. (Note: This is resolved through and extra database query)",
      resolve: parent => books.find({
        authorId: parent.authorId
      })
    }
  })
});
const rootQueryType = new _graphql.GraphQLObjectType({
  name: "root_query",
  description: "This is the root query for node_graphql_mongo_docker api",
  fields: () => ({
    book: {
      type: BookType,
      description: "Returns the book with the id passed",
      args: {
        id: {
          type: _graphql.GraphQLString
        }
      },
      resolve: (parent, id) => {
        const oid = new _mongodb.ObjectId(id);
        return books.findOne({
          ObjectId: oid
        });
      }
    },
    books: {
      type: (0, _graphql.GraphQLList)(BookType),
      description: "Returns a list of all books",
      resolve: () => books.find()
    },
    author: {
      type: AuthorType,
      description: "Returns the author that matches the authorId passed",
      args: {
        authorId: {
          type: _graphql.GraphQLInt
        }
      },
      resolve: (parent, authorId) => authors.findOne({
        authorId
      })
    },
    authors: {
      type: (0, _graphql.GraphQLList)(AuthorType),
      description: "Returns all authors",
      resolve: () => authors.find()
    }
  })
});
const rootMutationType = new _graphql.GraphQLObjectType({
  name: "root_mutation",
  description: "This is the root mutation for node_graphql_mongo_docker api",
  fields: () => ({
    addBook: {
      type: BookType,
      description: "Adds a book to the db. Takes name and authorId args (both non-null). Returns the added book.",
      args: {
        name: {
          type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLString),
          description: "Name of the new book"
        },
        authorId: {
          type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLInt),
          description: "New books authorId"
        }
      },
      resolve: (parent, args) => {
        const newBook = {
          name: args.name,
          authorId: args.authorId
        };
        books.create(newBook).then();
        return newBook;
      }
    }
  })
});
const schema = new _graphql.GraphQLSchema({
  query: rootQueryType,
  mutation: rootMutationType
});
router.use("/api", (0, _expressGraphql.default)({
  graphiql: true,
  schema
}));
var _default = router;
exports.default = _default;

//# sourceMappingURL=index.js.map