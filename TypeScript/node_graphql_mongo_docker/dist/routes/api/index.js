"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = _interopRequireDefault(require("express"));

var _expressGraphql = _interopRequireDefault(require("express-graphql"));

var _authors = _interopRequireDefault(require("./authors"));

var _books = _interopRequireDefault(require("./books"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const router = _express.default.Router();

router.use("/authors", _authors.default);
router.use("/books", _books.default);
router.use("/graphiql", (0, _expressGraphql.default)({
  graphiql: true,
  schema
}));
var _default = router;
exports.default = _default;

//# sourceMappingURL=index.js.map