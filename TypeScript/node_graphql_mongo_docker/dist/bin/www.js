"use strict";

var _app = _interopRequireDefault(require("../app"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//// Any Startup Scripts Here ////
const PORT = process.env.PORT || 8080;

_app.default.listen(PORT, () => console.log(`Server running on port: ${PORT}`)); //// Server error handling here ////
//// e.g.  app.on('error', onError); ////

//# sourceMappingURL=www.js.map