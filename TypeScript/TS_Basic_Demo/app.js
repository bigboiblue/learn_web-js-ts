"use strict";
exports.__esModule = true;
var express = require("express");
var app = express();
app.use(require("./routes"));
app.use(express.static("./public"));
module.exports = app;
