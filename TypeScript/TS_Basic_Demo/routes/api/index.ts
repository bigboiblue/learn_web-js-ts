import express = require("express");
const router = express.Router();

export * from "../index";

router.get("/", (request, response): void => {
    response.write(request.header("host"));
    response.write("\nHello");
    response.end();
});

interface Person {
    // lName: string; Interfaces shouldnt define private vars. Only implementation details
    // fName: string;
    fullName: string;

    printInfo: () => void;
    getInfo: () => string;
}

class Student implements Person {
    private readonly lName: string;
    private readonly fName: string;

    public readonly fullName: string;

    constructor(fName: string, lName: string) {
        this.fName = fName;
        this.lName = lName;
        this.fullName = `${this.fName} ${this.lName}`;
    }

    printInfo(arg: string = "hello"): void { // By default members are public
        console.log(`${this.fullName} ${arg}`);
    }

    getInfo(): string {
        return this.fullName;
    }
}

const student = new Student("Gregory", "Jones");
function personFunc(arg: Person): string {
    return arg.fullName;
}

console.log(personFunc(student));

type tuple = [string, number?, boolean?];
const tupleArr: tuple[] = [["Hello", 1, true], ["by", 1]];

interface Pair<T, U> { first: T; second?: U }
const myPair: Pair<number, null> = { first: 1 };

module.exports = router;
