"use strict";
exports.__esModule = true;
var express = require("express");
var router = express.Router();
router.get("/", function (request, response) {
    response.write(request.header("host"));
    response.write("\nHello");
    response.end();
});
var Student = /** @class */ (function () {
    function Student(fName, lName) {
        this.fName = fName;
        this.lName = lName;
        this.fullName = this.fName + " " + this.lName;
    }
    Student.prototype.printInfo = function (arg) {
        if (arg === void 0) { arg = "hello"; }
        console.log(this.fullName + " " + arg);
    };
    Student.prototype.getInfo = function () {
        return this.fullName;
    };
    return Student;
}());
var student = new Student("Gregory", "Jones");
function personFunc(arg) {
    return arg.fullName;
}
console.log(personFunc(student));
var tupleArr = [["Hello", 1, true], ["by", 1]];
var myPair = { first: 1 };
module.exports = router;
