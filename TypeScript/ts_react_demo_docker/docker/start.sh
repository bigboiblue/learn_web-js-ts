#!/bin/bash

if [ $# -le 0 ]; then
	echo "Must pass in the name of new container as an argument. e.g."
	echo "/docker/start.sh <my-container-name>"
	exit 1
fi

exec docker start -ai $1