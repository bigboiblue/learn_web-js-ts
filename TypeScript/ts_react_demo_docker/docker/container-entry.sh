#!/bin/bash

# $# is number of args passed.
# $@ is array of args passed.
if [ $# -gt 0 ]; then
	# Doing this allows us to use ENTRYPOINT as if CMD with extra functionality
	exec "$@"
else
	echo "Executing keep alive script..."
	echo "Its important to rerun this in a detached state, then attach manually to access the terminal."
	echo "This is because this runs script runs infinitely to allow attaching to terminal easily without creating a new container."

	exec /bin/zsh
	
	# if [ "$(ls ./node_modules/)" = "" ]; then
	# 	yarn
	# fi

	# wp="webpack-dev-server"

	# echo "Starting yarn on port ${PORT}..."
	# exec ./node_modules/${wp}/bin/${wp}.js --port=${PORT} --host=0.0.0.0
fi
