#!/bin/bash

if [ $# -le 0 ]; then
	echo "Must pass in the name of new container as an argument. e.g."
	echo "/docker/create.sh <my-new-container-name>"
	exit 1
fi

image_name="bigboiblue/react_demo:v1.0.0"

echo $image_name

# $() creates runs contents in a sub-shell
docker run --rm -it \
	--name $1 \
	-p 8080:8080 \
	-e PORT=8080 \
	-v "$(pwd):/app" \
	-v "/app/docker" \
	-v "/app/.git" \
	-v "$(pwd)/docker/container-entry.sh:/app/docker/container-entry.sh" \
	$image_name
	# "-v app/vscode": This mounts an empty volume, essentially excluding vscode folder