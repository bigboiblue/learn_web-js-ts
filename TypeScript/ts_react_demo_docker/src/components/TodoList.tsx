import React from "react";
import * as types from "../typings/types";
import TodoItem from "./TodoItem";
import "./scss/TodoList.scss";

interface PropTypes {
    todos: types.Todo[];

    // functions
    markComplete: (id: number) => void;
    removeTodo: (id: number) => void;
}

class TodoList extends React.Component {
    props: PropTypes;

    render(): React.ReactElement {
        const list = this.props.todos.map((todo): React.ReactElement => (
            <TodoItem
                todo={todo}
                key={todo.id}
                markComplete={this.props.markComplete}
                removeTodo={this.props.removeTodo}
            />
        ));
        return (<div className="TodoList">{list}</div>);
    }
}

export default TodoList;
