import React from "react";

function About(): React.ReactElement {
    return (
        // Like a ghost element. Doesnt appear in dom, but is needed for JSX to render
        // Multiple elements
        <React.Fragment>
            <h1>About</h1>
            <p>This is our about page for our first typescript react app</p>
        </React.Fragment>
    );
}

export default About;
