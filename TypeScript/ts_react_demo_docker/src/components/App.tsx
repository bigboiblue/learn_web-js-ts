import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import * as types from "../typings/types";
import TodoList from "./TodoList";
import Header from "./layout/Header";
import AddTodo from "./AddTodo";
import About from "./pages/About";

interface State {
    todos: types.Todo[];
}

// Convention to use PascalCase for Components and corresponding filenames
class App extends React.Component {
    state: State = {
        todos: [
            {
                id: 1,
                title: "Take out trash",
                completed: false,
            },
            {
                id: 2,
                title: "Feed the cat",
                completed: false,
            },
            {
                id: 3,
                title: "Have a nap",
                completed: false,
            },
        ],
    };

    markComplete = (id: number): void => {
        this.setState({
            todos: this.state.todos.map((val): types.Todo => {
                const temp = val;
                if (val.id === id) {
                    temp.completed = !val.completed;
                }
                return temp;
            }),
        });
    }

    removeTodo = (id: number): void => {
        const newTodos = this.state.todos.filter((val): boolean => !(val.id === id));
        for (let i = 0; i < newTodos.length; ++i) {
            newTodos[i].id = i + 1;
        }
        this.setState({ todos: newTodos });
    }

    addTodo = (title: string): void => {
        const newTodo = {
            id: this.state.todos.length + 1,
            title,
            completed: false,
        };
        this.setState({ todos: [...this.state.todos, newTodo] });
    }

    render(): React.ReactElement {
        return (
            // To Allow certain elements to stay and change per page, use a BrowserRouter
            <BrowserRouter basename="/">
                <Header />

                {/* This route renders multiple components */}
                <Route exact path="/" render={
                    (/*props*/): React.ReactElement => (
                        <React.Fragment>
                            <TodoList
                                todos={this.state.todos}
                                markComplete={this.markComplete}
                                removeTodo={this.removeTodo}
                            />
                            <AddTodo addTodo={this.addTodo} />
                        </React.Fragment>
                    )
                } />

                {/* Component Route. Much easier */}
                <Route path="/about" component={About} />

            </BrowserRouter>

        );
    }
}

export default App;
