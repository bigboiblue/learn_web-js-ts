import Vue from "vue";
import App from "./App.vue";

// A Vue applicaiton starts with a root Vue instance
// (All component files are transpiled into Vue instances with webpacks vue-loader)
let vm = new Vue({
	// Render returns a vNode (a VirtualDOM node / custom element)
	render: createElement => createElement(App),
});
vm.$mount("#app");
