// Webpack config must be in CommonJS unfortunately
const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require("path");

module.exports = {
	entry: {
		app: path.resolve(__dirname, "src/index.tsx"), polyfill: "@babel/polyfill",
	},
	output: {
		path: path.resolve(__dirname, "dist"),
		filename: "[name]_bundle.js",

		// This allow routing in application (so multiple bundles are created) [Research Why]
		publicPath: "/",
	},
	module: {
		rules: [
			// Scss to css string, then inject into html
			{
				test: /\.scss$/,
				loaders: ["style-loader", "css-loader", "sass-loader"],
			},

			// General image file loader
			{
				test: /\.(png|jpg|gif|svg)$/,
				loader: "file-loader",
				options: {
					name: "[name].[ext]?[hash]",
				},
			},

			// No longer needed. Typechecking through no emit command after build for speed
			// // Ts to js es5
			// {
			//     test: /\.tsx?$/,
			//     exclude: path.resolve(__dirname, "node_modules"),
			//     loaders: ["babel-loader", "ts-loader"],
			// },

			// Ts to js es5
			{
				test: /\.[tj]sx?$/,
				exclude: path.resolve(__dirname, "node_modules"),
				loader: "babel-loader",
			},


		],
	},

	devtool: "cheap-module-source-map",
	mode: "development",

	devServer: {
		historyApiFallback: true,
		hot: true,
		noInfo: true,
	},

	// Automatically resolve these file types without
	// explicit declaration of type in import statement
	resolve: {
		extensions: [".js", ".jsx", ".ts", ".tsx"],
	},

	plugins: [
		new HtmlWebpackPlugin(
			{ template: path.resolve(__dirname, "src/index.html") },
		),
	],
};


// Production Mode Optimisations
if (process.env.NODE_ENV === "production") {
	module.exports.devtool = "#source-map";
	module.exports.plugins = (module.exports.plugins || []).concat([
		new webpack.DefinePlugin({
			"process.env": {
				NODE_ENV: "\"production\"",
			},
		}),
		new webpack.optimize.UglifyJsPlugin({
			sourceMap: true,
			compress: {
				warnings: false,
			},
		}),
		new webpack.LoaderOptionsPlugin({
			minimize: true,
		}),
	]);
}
