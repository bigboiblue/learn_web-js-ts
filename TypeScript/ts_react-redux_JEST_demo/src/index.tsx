import "@babel/polyfill";

import React from "react";
import ReactDom from "react-dom";
import "./scss/index_main.scss";

import {
	createStore, combineReducers, compose, applyMiddleware,
} from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import allReducer from "./reducers";

import App from "./components/App";


const initialState = {
	users: [{ name: "Greg", age: 19 }],
	products: [{ title: "xbox 360", price: 21.90 }],
	todos: [
		{
			id: 1,
			title: "Take out rubbish",
			completed: false,
		},
		{
			id: 2,
			title: "Feed the cat",
			completed: false,
		},
		{
			id: 3,
			title: "Have a nap",
			completed: false,
		},
	],
};

// compose simply combines functions from right to left (so applymiddleware called, then extension)
const storeEnhancers: any = compose(
	applyMiddleware(thunk),
	// @ts-ignore
	window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
);

// Arg 1 = reducer, arg 2 = intial state, arg 3 = does dev extension exist?
const store = createStore(allReducer,
	initialState,
	// store enhancers are simply an addon (or middleware) that may enhandce capabilities of the store
	storeEnhancers);

// This is how you send an action to reducer in this store.
// store.dispatch(action);

// Get full store state object
// console.log(store.getState());

ReactDom.render(<Provider store={store}> <App /> </Provider>, document.getElementById("app"));
