// In jest, setup and teardown doesnt work quite like googletest.
// gtest has fixtures, classes shared by tests, but jest doesnt. Jest has
// setup and teardown functions (beforeEach, afterEach, and beforeAll, afterAll).
// remember, these are just functions, so you cannot access their state from
// the tests. Their purpose is to initialise state elsewhere (possibly global,
// which is bad practice). Instead, I could simple make my own classes.

// beforeEach and afterEach are used to do something before each test runs in its scope
beforeEach(() => {
	console.log("Intialise something here...");
});
afterEach(() => {
	console.log("Clear / reset some data here");
});

// beforeAll and afterAll are used to do something before all tests run in its scope
beforeAll(() => {
	console.log("Initialise something once once overall for all tests here...");
});
afterAll(() => {
	console.log("Clear / reset some data only once overall for all tests here");
});

// By default, the scope is the entire file (or test suite) To limit scope, and group
// tests, we can use a describe block.
beforeAll(() => {
	console.log("I wont apply to the GREGMAS scope / group");
});

describe("GREGMAS", () => {
	beforeAll(() => {
		console.log("I will apply to the GREGMAS scope / group");
	});

	test("I am greg", () => {
		console.log("Im old greg");
	});
});
