import "@babel/polyfill";

// More comments in "jest_mocking_rubbish_only_1_ret_val.test.jsx"
// Here we use mock return values (e.g. mockReturnValueOnce(x))
class Database {
	public connect: any = async (username: string, password: string) => {
	}
}

// NOTE: Instead of creating the mock manually such as done here, we can actually
// convert a depencey into a mock class using jest.mock("dependency-name")
// e.g. import axios from "axios";
//      jest.mock("axios");
//      axios.get.mockResolvedValue(x);
//      axios.get.mockRejectedValue(y); etc.
class MyMockClass extends Database {
	// Function does nothing in this case. We are going to mock the functionality later
	// any is used on the function here to allow mock to return anything in mockReturnValue.
	// If the return type is known I would specify.
	public connect = jest.fn((username: string, password: string): any => {});
}


test("mock connect", () => {
	const myMockDatabase = new MyMockClass();

	myMockDatabase.connect
		.mockReturnValueOnce("Login Successful")
		.mockReturnValueOnce("Login Failure")
		.mockReturnValue("Please enter login details...");

	// A stub wouldnt care how we got the data (e.g. the args passed)
	expect(myMockDatabase.connect("a", "b")).toBe("Login Successful"); // Login Successful
	expect(myMockDatabase.connect("c", "d")).toBe("Login Failure"); // Login Failure
	expect(myMockDatabase.connect("e", "f")).toBe("Please enter login details..."); // Please enter login details...
	expect(myMockDatabase.connect("g", "h")).toBe("Please enter login details..."); // Please enter login details...

	// But a mock does, so name your vars correctly
	// Checks on the 3 call of connect that 2nd arg is 'f'
	expect(myMockDatabase.connect.mock.calls[2][1]).toBe("f");
});


// Still, there are cases where it's useful to go beyond the ability to specify return
// values and full-on replace the implementation of a mock function. This can be done
// with jest.fn or the mockImplementationOnce method on mock functions.

// We can also reimpleement the mock implementation using mockImplementationOnce
// (and simply mockImplementation)
// The first 2 calls will be "first" and "second". After, "default" will be returned.
test("mock connect reimplementation", () => {
	const connectMock = jest.fn(() => "Default")
		.mockImplementationOnce(() => "first")
		.mockImplementationOnce(() => "second");
});

