// Could setup jest extension, OR run jest --watch in gulpfile

test("Hello World", () => {
	expect(true).toBe(true);
});

test("Object Equality", () => {
	const myObject = { fName: "Kyle", lName: "Nuttall" };
	// expect(myObject).toBe({ fName: "Kyle", lName: "Nuttall" })
	// Above assertion will be false. Objects and arrays are reference types.
	// They are technically different when compared due to memory location, despite equal members
	// Because of this, we must instead use the below syntax (which iteratively checks for equality)
	expect(myObject).toStrictEqual({ fName: "Kyle", lName: "Nuttall" });
	// toEqual also works, though toStrictEqual also checks for type (same as dif between == and ===)
});

test("Less Than More than", () => {
	expect(1).toBeLessThanOrEqual(2);
});

// REGEX
test("There is no I in team", () => {
	expect("team").not.toMatch(/I/i);
});

// Ther are array matchers
test("Admin should be in users", () => {
	const users = ["johen", "greg", "admin"];
	expect(users).toContain("admin");
});

/*
	MATCHERS
	(basically non-fatal assertions) [These test the evaluated value in expect argument]

	expect(someFunc()).toBe(x) -> someFunc must eval to x
	toBeNull() matches only null
	toBeUndefined() matches only undefined
	toBeDefined() matches only anything that is defined (basically anything not undefined)
	toBeTruthy() matches anything an if statement treats as true
	toBeFalsey() matches anything an if statement treats as false

	There are MANY more matchers... Check the docs.

	(we can also use expect(true).not.toBeFalsey())
*/
