import React, { ReactElement } from "react";
import "./scss/header.scss";
import { Link } from "react-router-dom";

function Header(/* props: Props */): ReactElement {
    return (
        <header>
            <h1>Todo App</h1>
            <Link className="link" to="/">Home</Link> | <Link className="link" to="/about">About</Link>
        </header>
    );
}

export default Header;
