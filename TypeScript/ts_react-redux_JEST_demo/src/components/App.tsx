import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import TodoList from "./TodoList/TodoList";
import Header from "./Header";
import AddTodo from "./TodoList/AddTodo";
import About from "./About";
import { addTodoAction, fetchTodosAction } from "../actions/todoListActions";


interface State {
	todos: Todo[];
}

// Convention to use PascalCase for Components and corresponding filenames
class App extends React.Component {
	// markComplete = (id: number): void => {
	// 	this.setState({
	// 		todos: this.state.todos.map((val): types.Todo => {
	// 			const temp = val;
	// 			if (val.id === id) {
	// 				temp.completed = !val.completed;
	// 			}
	// 			return temp;
	// 		}),
	// 	});
	// }

	// removeTodo = (id: number): void => {
	// 	const newTodos = this.state.todos.filter((val): boolean => !(val.id === id));
	// 	for (let i = 0; i < newTodos.length; ++i) {
	// 		newTodos[i].id = i + 1;
	// 	}
	// 	this.setState({ todos: newTodos });
	// }

	// addTodo = (title: string): void => {
	// 	const newTodo = {
	// 		id: this.state.todos.length + 1,
	// 		title,
	// 		completed: false,
	// 	};
	// 	this.setState({ todos: [...this.state.todos, newTodo] });
	// }
	props: any;

	render(): React.ReactElement {
		return (
			// To Allow certain elements to stay and change per page, use a BrowserRouter
			<BrowserRouter basename="/">
				<Header />

				{/* This route renders multiple components */}
				<Route exact path="/" render={
					(/*props*/): React.ReactElement => (
						<React.Fragment>
							<TodoList/>
							<AddTodo /*addTodo={this.addTodo} */ />
							<button onClick={this.props.fetchTodos}>Fetch More Todos From Typicode</button>
						</React.Fragment>
					)
				} />

				{/* Component Route. Much easier */}
				<Route path="/about" component={About} />

			</BrowserRouter>

		);
	}
}

// Maps state from a redux store to props within this component
const mapStateToProps = state => ({
	users: state.users,
	products: state.products,
});

// Maps actions to props that can be used as functions in component rather
// than messing around with store.dispatch() calls.
const mapDispatchToProps = (dispatch, props) => bindActionCreators({
	addTodo: addTodoAction,
	fetchTodos: fetchTodosAction,
}, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(App);
