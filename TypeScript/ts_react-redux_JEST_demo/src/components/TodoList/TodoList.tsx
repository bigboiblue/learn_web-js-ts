import React from "react";
import { connect } from "react-redux";
import TodoItem from "./TodoItem";
import { selectFullStrings } from "../../reducers";
import "../scss/TodoList.scss";


interface PropTypes {
	todos: Todo[];
	fullStrings: any;
}

class TodoList extends React.Component {
	props: PropTypes;

	render(): React.ReactElement {
		let { todos } = this.props;
		let list = [];
		for (let i = 0; i < this.props.todos.length; ++i) {
			list.push(
				<TodoItem key={todos[i].id} todo={todos[i]} fullString={this.props.fullStrings[i]} />,
			);
		}
		return (<div className="TodoList">{list}</div>);
	}
}

const mapStateToProps = (state, ownProps) => ({
	todos: state.todos,
	fullStrings: selectFullStrings(state),
});

export default connect(mapStateToProps)(TodoList);
