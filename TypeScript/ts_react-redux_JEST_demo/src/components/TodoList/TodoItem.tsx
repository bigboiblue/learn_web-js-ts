import React, { Component } from "react";
import { connect } from "react-redux";
import "../scss/TodoItem.scss";
import { bindActionCreators } from "redux";
import { addTodoAction, removeTodoAction, markDoneAction } from "../../actions/todoListActions";

export class TodoItem extends Component {
	props: {todo: {id, completed, title, }, markDone, removeTodo, fullString, };

	state = {
		completed: this.props.todo.completed,
	}

	markDone = (e) => {
		this.setState({ completed: !this.state.completed });
		this.props.markDone(this.props.todo.id);
	}

	// prop validation done here (in typescript)
	render() {
		let { id, title, completed } = this.props.todo;
		return (
			<div className={`TodoItem${this.state.completed ? "Complete" : "Incomplete"}`}>
				<input type="checkbox" onChange={this.markDone} />
				{this.props.fullString}
				<button onClick={this.props.removeTodo.bind(null, id)}>X</button>
			</div>
		);
	}
}
function mapDispatchToProps(dispatch, props) {
	// bindActionCreators turns an object whos values are action creators into an object with the
	// same keys, but every action creator is wrapped as an arg to a dispatch call so they can
	// be invoked directly
	return bindActionCreators({
		removeTodo: removeTodoAction,
		markDone: markDoneAction,
	}, dispatch);
}
export default connect(null, mapDispatchToProps)(TodoItem);
