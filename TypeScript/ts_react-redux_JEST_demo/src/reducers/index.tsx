import { combineReducers } from "redux";
import { createSelector } from "reselect";
import productReducer from "./productReducer";
import todoListReducer, * as fromTodoList from "./todoListReducer";
import userReducer from "./userReducer";


// Allows you to combine more than one reducer (since a store can only have 1 main reducer)
// Convention to combine reducers in an index file
const allReducer = combineReducers({
	users: userReducer,
	todos: todoListReducer,
	products: productReducer,
});

// This is only a private utility function for reselects createSelector
// Usually we would be combining more than 1 piece of data
const selectTodos = state => state.todos;

// Now that we are using createSelector from reselect, our selectors are "memoized".
// This means that if our required state args (aquired from selectTodos) does not
// change, then a cached version of a previous return value is returned as there
// is no need to recalculate the derived data.
export const selectFullStrings = createSelector(
	selectTodos,
	todos => fromTodoList.selectFullStrings(todos),
);
// Note, the above selector is composable; it can be used in another createSelector as an arg


// DEFINITION: In computing, memoization or memoisation is an optimization technique used primarily
// to speed up computer programs by storing the results of expensive function calls and returning
// the cached result when the same inputs occur again.

export default allReducer;
