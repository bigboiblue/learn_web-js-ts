function productReducer(state: Product[] = [], { type, payload }: any):
Product[] | Product {
	switch (type) {
	case "updateProduct":
		return payload;
	default:
		return state;
	}
}
export default productReducer;
