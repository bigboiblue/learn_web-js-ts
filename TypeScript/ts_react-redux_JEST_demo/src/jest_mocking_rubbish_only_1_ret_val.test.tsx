import "@babel/polyfill";

// This is the class I want to test
class Database {
	// We need to allow the type to be any here to allow the override function to
	// change to jest type
	public connect: any = async (username: string, password: string) => {
		// Since this is an example, there is no actual implementation
	}
}

// We created a mock class, mocking all the functions (we dont necessarily need to
// do this, if we wanted we could simply mock each function within the test without a class)
class MyMockClass extends Database {
	public connect = jest.fn((username: string, password: string) => "Login successful");

	//This function is only made for purpose of example
	public connect5Times(): void {
		for (let i = 1; i <= 5; ++i) {
			this.connect(`user ${i}`, "");
		}
	}
}


test("mock connect", () => {
	const myMockDatabase = new MyMockClass();
	myMockDatabase.connect5Times();

	// Expect connect to be called 5 times
	expect(myMockDatabase.connect.mock.calls.length).toBe(5);

	// Expect on first call, the first arg toBe "user 1"
	// (first dimension of array is call number, second is arg number)
	expect(myMockDatabase.connect.mock.calls[0][0]).toBe("user 1");

	//Expect on the first call, the return value (can also check type) is "Login successful"
	expect(myMockDatabase.connect.mock.results[0].value).toBe("Login successful");
});

