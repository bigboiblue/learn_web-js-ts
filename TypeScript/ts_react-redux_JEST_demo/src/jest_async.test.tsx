import fs from "fs-extra";
import path from "path";
import "@babel/polyfill";

// Expect.assertions(number) verifies that a certain number of assertions are called during a test.
// This is often useful when testing asynchronous code, in order to make sure that assertions in a
// callback actually got called
// NOTE: this is only needed when the promise is meant to reject (e.g. catch). This is because
// generally catch blocks are skipped

// Async tests often pass wrongly because the intended assertions were not made before the
// test-runner (Jest,Mocha etc.) thought the test was finished.

// Callbacks must pass a function with one arg (done) which must be called after the
// callback has finished. if it done() is not called after a preset timeout, the test fails.

const myVal = "Hello";
test("callback test", (done) => {
	fs.readFile(path.join(__dirname, "index.tsx"), (err, data) => {
		expect(err).toBeFalsy();
		if (!err) {
			console.log("Read all data from index.tsx");
			expect(myVal).toBe("Hello");
			done();
		}
	});
});


// The passed function must return a promise to test async promises
test("Promise test", () => fs.readFile(path.join(__dirname, "index.tsx"))
	.then((data) => {
		expect(data).toBeDefined();
		expect(data).toBeTruthy();
	}));

// We can also use the "resolves" and "rejects" matcher like so to handle tests with promises
test("second promise test", () => {
	const filePath = path.join(__dirname, "index.tsx");
	// Only passes when promise resolves (then)
	return expect(fs.readFile(filePath)).resolves.toBeDefined(); // Defines data
}); //Also note, you still need to return the promise


// Only passes when promise rejects (catch)
// Also, expect.assertions(1) not needed here, since we use "rejects"
test("second promise test", () => expect(fs.readFile("greg")).rejects.toBeDefined()); // Defines err


// "async" functions return promise by default, so these already work in testing
test("async await test", async () => {
	const val = await fs.readFile(path.join(__dirname, "index.tsx"));
	expect(val).toBeDefined();

	// We can also use rejects
	await expect(fs.readFile("esfdwadwa")).rejects.toBeDefined();
});


test("expect.assertions", () => {
	// Required when using catch blocks or promise.catch() since catch blocks are skipped
	expect.assertions(1);
	return fs.readFile("esfdwadwa")
		.catch(err => expect(err).toBeDefined());
});

