import React, { Component } from "react";
import { StatusBar } from "react-native";
import { registerRootComponent } from "expo";
import { activateKeepAwake } from "expo-keep-awake";

import Navbar from "./components/Navbar";
import HomeScreen from "./components/HomeScreen";
import CustomStatusBar from "./components/CustomStatusBar";

if (__DEV__) {
	activateKeepAwake();
}

class App extends Component {
	render(): React.ReactElement { // eslint-disable-line
		return (
			<React.Fragment>
				<CustomStatusBar backgroundColor={"#333"} barStyle="light-content" />
				<Navbar />
				<HomeScreen />
			</React.Fragment>
		);
	}
}

// @ts-ignore
export default registerRootComponent(App);

