import React, { Component } from "react";
import {
	StyleSheet, View, Text, StatusBar,
} from "react-native";

interface Props {

}

function HomeScreen(props: Props): React.ReactElement {
	const lorem30 = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro adipisci velit recusandae aliquam esse quas officiis veniam, id neque? Illo, possimus laudantium pariatur beatae ducimus molestias eum eaque cum nostrum.";

	return (
		<View style={style.container}>
			<Text style={style.title}>Home</Text>
			<Text style={style.body}>{lorem30}</Text>
		</View>
	);
}

const style = StyleSheet.create({
	container: {
		backgroundColor: "#fff",
		color: "black",
		height: "100%",
		width: "100%",
		padding: 30,
	},
	title: {
		textDecorationLine: "underline",
		fontSize: 30,
		textAlign: "center",
		fontWeight: "bold",
	},
	body: {
		fontSize: 18,
		textAlign: "justify",
	},
});

console.log(`Height: ${StatusBar.currentHeight}`);

export default HomeScreen;
