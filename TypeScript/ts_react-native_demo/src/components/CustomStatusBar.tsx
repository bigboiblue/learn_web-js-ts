import React, { Component } from "react";
import {
	View, StatusBar, StyleSheet, Platform, Text, Dimensions,
} from "react-native";
// Wait for expo to update to accomodate for react-native@0.6.0 as this allows auto linking
import DeviceInfo from "react-native-device-info";

interface Props {
	backgroundColor: string;
	barStyle: "default" | "light-content" | "dark-content";
}

class CustomStatusBar extends Component {
	props: Props;

	render() {
		return (
			<React.Fragment>
				<StatusBar
					translucent={true}
					barStyle={this.props.barStyle}
					backgroundColor={this.props.backgroundColor}
				/>
				<View style={this.style.statusBar}>
				</View>
			</React.Fragment>
		);
	}

	// Stylesheet.create ensures the stylesheet is only loaded once if included in many places
	// However, we simply referring to an object without StyleSheet.create() will work.
	// This means that we can create dynamic styles by making functions that return stylesheets
	style = StyleSheet.create({
		statusBar: {
			backgroundColor: this.props.backgroundColor,
			height: getStatusBarHeight(), // eslint-disable-line
			display: "flex",
		},
	});
}

enum statusBarHeight { IPHONE_X = 44, IPHONE_OTHER = 20 }

function getStatusBarHeight(): number {
	switch (Platform.OS) {
	case "ios":
		// TODO: Link DeviceInfo Module
		if (/* DeviceInfo.getModel().includes("iPhone X") */ false) { // eslint-disable-line
			return statusBarHeight.IPHONE_X;
		}
		return statusBarHeight.IPHONE_OTHER;

	case "android":
		return StatusBar.currentHeight;

	default:
		return 0;
	}
}


export default CustomStatusBar;
