import React from "react";
import ReactDom from "react-dom";
import "./scss/index_main.scss";

import App from "./components/App";

ReactDom.render(<App />, document.getElementById("app"));
