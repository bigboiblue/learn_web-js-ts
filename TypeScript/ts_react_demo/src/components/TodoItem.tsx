import React, { Component } from "react";
import * as types from "../typings/types";
import "./scss/TodoItem.scss";

interface Props {
    todo: types.Todo;

    // functions
    markComplete: (id) => void;
    removeTodo: (id) => void;
}


export class TodoItem extends Component {
    props: Props;

    // Since React.Component doesnt auto bind methods to itself, we must bind them ourselves
    // This can be done through the construter (see comment in ctor) or using an arrow func like so
    // markComplete = (event): void => {
    //     console.log(this.props);
    //     console.log(event);
    // }

    // constructor(props) {
    //     super(props);
    //     this.markComplete = this.markComplete.bind(this);
    // }

    render(): React.ReactElement {
        const { id, title, completed } = this.props.todo;
        return (
            <div className={`TodoItem${completed ? "Complete" : "Incomplete"}`}>
                <input type="checkbox" onChange={this.props.markComplete.bind(null, id)} />
                {id}. {title}
                <button onClick={this.props.removeTodo.bind(null, id)}>X</button>
            </div>
        );
    }
}

export default TodoItem;
