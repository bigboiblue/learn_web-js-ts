import React, { Component } from "react";

interface Props {
    // Functions
    addTodo: (title: string) => void;
}
interface State {
    [key: string]: string;
}

export default class AddTodo extends Component<Props, State> {
    state = {
        title: "",
    }

    onChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({ [e.target.name]: e.target.value });
    }

    addTodo = (e: React.FormEvent<HTMLFormElement>): void => {
        e.preventDefault(); // Stop form submit reloading page
        this.props.addTodo(this.state.title);
        this.setState({ title: "" });
    }

    render(): React.ReactElement {
        return (
            <form onSubmit={this.addTodo}>
                <input
                    type="text"
                    name="title"
                    id="newTodo"
                    placeholder="New Todo..."
                    onChange={this.onChange}
                    value={this.state.title}
                />
            </form>
        );
    }
}
